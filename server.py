from tornado.ioloop import IOLoop
from tornado.iostream import IOStream, StreamClosedError
from tornado.tcpserver import TCPServer
from tornado.web import RequestHandler, Application
from typing import Optional, Awaitable
import logging
import eventparser


class TrackerServer(TCPServer):
    async def handle_stream(self, stream: IOStream, address: tuple) -> Optional[Awaitable[None]]:
        while True:
            try:
                data = await stream.read_until_close()
                print("Recibiendo datos")
                decoded_data: str = bytes.decode(data, encoding='ascii')
                logging.debug(decoded_data)
                eventparser.TrackerString.create(string_data=decoded_data)
                if(decoded_data.startswith('+RESP:GTSOS')):
                    # logging.debug(decoded_data)
                    event = eventparser.parse_event(decoded_data)
                    eventparser.save_event(event)
                # eventparser.update_bus(event)

            except StreamClosedError:
                break


class IndexHandler(RequestHandler):
    def get(self):
        buses = eventparser.GL300Bus.select()
        res = []
        for bus in buses:
            res.append(dict(
                name=bus.name,
                hexcolor=bus.hexcolor,
                lat=bus.latitude,
                long=bus.longitude,
            ))
        self.write({"data":res})


if(__name__ == "__main__"):
    server = TrackerServer()
    # webserver = Application([
    #     (r"/", IndexHandler),
    # ])
    logging.debug("Tracker server created")
    print("Starting tornado TCP server on port 9999")
    server.listen(9999)
    # webserver.listen(8888)
    IOLoop.current().start()
