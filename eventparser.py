import csv
from typing import List
from peewee import *
import datetime
import logging
logging.basicConfig(level=logging.DEBUG)
# db = SqliteDatabase("tracking.db")
db = MySQLDatabase("tracker", user="tracker", password="tracker", host="localhost", charset='utf8mb4')

class Device(object):
    def __init__(self, imei: str, name: str, active: bool = False):
        self.imei = imei
        self.name = name
        self.active = active
        self.longitude = 0
        self.latitude = 0


class GL300(Device):
    def __init__(self, imei: str, name: str = "GL300", active: bool = False):
        self.protocol_version = 0
        self.count_number = 0
        self.battery_percentage = 0
        self.active = active
        super().__init__(imei, name, active)


event_types = [
    "+RESP",    # report
    "+ACK",     # acknowledgement by device
]


class GPSPoint(object):
    def __init__(self, accuracy: int, speed: int, azimuth: int, altitude: int, longitude: int, latitude: int, timestamp: datetime.datetime):
        self.accuracy = accuracy
        self.speed = speed
        self.azimuth = azimuth
        self.altitude = altitude
        self.longitude = longitude
        self.latitude = latitude
        self.timestamp = timestamp


class TrackerEvent(object):
    def __init__(self, event_type: str, timestamp: datetime.datetime, device: Device, points: List[GPSPoint]):
        self.event_type = event_type
        self.timestamp = timestamp
        self.device = device
        self.points = points


class TrackerString(Model):
    string_data = CharField(null=False)
    string_timestamp = DateTimeField(default=datetime.datetime.now)
    class Meta:
        database = db


class TrackerDB(Model):
    device_imei = CharField(null=False)
    device_name = CharField(null=False)
    device_time = TimeField(default=datetime.datetime.now)
    class Meta:
        database = db


class TrackerSOS(Model):
    sos_device = ForeignKeyField(TrackerDB, backref='alerts', on_delete='CASCADE')
    sos_latitude = CharField()
    sos_longitude = CharField()
    sos_timestamp = TimeField(default=datetime.datetime.now)
    sos_datestamp = DateField(default=datetime.datetime.now)
    sos_acuraccy = IntegerField()
    sos_battery = IntegerField()
    sos_count = IntegerField()
    sos_gmap = CharField()
    class Meta:
        database = db


class GL300Bus(Model):
    name = CharField()
    imei = CharField()
    latitude = CharField()
    longitude = CharField()
    hexcolor = CharField()
    updated = TimeField(default=datetime.datetime.now)

    class Meta:
        database = db


db.create_tables([GL300Bus, TrackerDB, TrackerSOS, TrackerString], safe=True)


def update_bus(event: TrackerEvent):
    gl, created = GL300Bus.get_or_create(
        imei=event.device.imei,
        defaults={
            "name": event.device.name,
            "latitude": event.points[0].latitude,
            "longitude": event.points[0].longitude,
            "hexcolor": "000000",
        }
    )
    if(not created):
        gl.latitude = event.points[0].latitude,
        gl.longitude = event.points[0].longitude,
    gl.updated = datetime.datetime.now()
    gl.save()


def parse_timestamp(timestamp: str) -> datetime.datetime:
    if(timestamp):
        return datetime.datetime.strptime(timestamp, "%Y%m%d%H%M%S")
    else:
        logging.warning("empty timestamp received, using server timestamp")
        return datetime.datetime.today().astimezone(datetime.timezone(-datetime.timedelta(hours=3)))


#  +ACK:GTFRI,300400,860599001191576,GL300,0009,20191120205133,C337$
#  +RESP:GTFRI,300400,860599001191576,GL300,0,0,1,3,7.7,101,11.5,-60.156333,-32.611548,20191120235152,0722,0007,09C5,0F3A,,91,20191120205153,C338$
def parse_event(data: str) -> TrackerEvent:
    params: List[str] = csv.reader([data], delimiter=',', quoting=csv.QUOTE_NONE).__next__()
    logging.debug("Params: {}".format(params))
    event_type = params[0]
    protocol_version = params[1]
    imei = params[2]
    device = GL300(imei, params[3])
    device.protocol_version = protocol_version
    device.count_number = int(params[-1].rstrip("$"), 16)
    points = list()
    if(event_type == "+RESP:GTSOS"):  # +RESP:GTFRI | +RESP:GTSOS
        device.battery_percentage = int(params[-3])
        gps_points: int = int(params[6])
        logging.debug("GPS Points: {}".format(gps_points))
        for i in range(1, gps_points + 1):
            # print("Parsing ", params[13 * i])
            gps_accuracy = params[7 * i]
            gps_speed = params[8 * i]
            gps_azimuth = params[9 * i]
            gps_altitude = params[10 * i]
            gps_longitude = params[11 * i]
            gps_latitude = params[12 * i]
            gps_utc_time = params[13 * i]
            logging.debug("UTC: {}".format(gps_utc_time))
            gps_mcc = params[14 * i]
            gps_mnc = params[15 * i]
            gps_lac = params[16 * i]
            gps_cell_id = params[17 * i]
            gps_odo_mileage = params[18 * i]
            params[13 * i] = parse_timestamp(params[13 * i])
            points.append(GPSPoint(*params[7 * i:14 * i]))
        if(len(points) > 0 and points[0].latitude and points[0].longitude):
            device.latitude = points[-1].latitude
            device.longitude = points[-1].longitude
        else:
            logging.warning("No GPS POS received")
            device.latitude = ''
            device.longitude = ''
    return TrackerEvent(event_type, parse_timestamp(params[-2]), device, points)

def save_event(event: TrackerEvent):
    try:
        device: TrackerDB = TrackerDB.get(device_imei=event.device.imei)
    except DoesNotExist:
        device = TrackerDB.create(
            device_imei=event.device.imei,
            device_name=event.device.name,
            device_time=event.timestamp)
    if(len(event.points) > 0 and event.points[0].latitude and event.points[0].longitude):
        gmap_url = "https://www.google.com/maps/search/?api=1&query={},{}".format(event.points[0].latitude, event.points[0].longitude)
    else:
        logging.warning("Received SOS with NO GPS POSITION")
        gmap_url = ""
    event: TrackerSOS = TrackerSOS.create(
        sos_device = device,
        sos_latitude = event.points[0].latitude,
        sos_longitude = event.points[0].longitude,
        sos_timestamp = event.timestamp,
        sos_datestamp = event.timestamp,
        sos_accuracy = event.points[0].accuracy,
        sos_battery = event.device.battery_percentage,
        sos_count = event.device.count_number,
        sos_gmap = gmap_url
        )
    event.save()

